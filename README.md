# Espoir Et Vie



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!


## Installation

```
apt update
apt -y install git ansible
ansible-pull -U https://gitlab.com/chriscowleysound/espoir-et-vie.git -i hosts
```
